const express = require('express')
const router = express.Router()
const authMiddleware = require('../middlewares/authMiddleware')
const { addNote, getNotes, getNoteById, updateNoteById, checkNote, deleteNoteById } = require('../controllers/notesController')


router.get('/notes', authMiddleware, getNotes)
router.post('/notes', authMiddleware, addNote)
router.get('/notes/:id', authMiddleware, getNoteById)
router.put('/notes/:id', authMiddleware, updateNoteById)
router.patch('/notes/:id', authMiddleware, checkNote)
router.delete('/notes/:id', authMiddleware, deleteNoteById)

module.exports = router

