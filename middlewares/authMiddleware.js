const jwt = require('jsonwebtoken')
const { secret } = require('../config/auth')

module.exports = (request, response, next) => {
    const [, token] = request.headers.authorization.split(' ');

    if(!token) {
        return response.status(401).json({message: 'No authorization header found'})
    }

    try {
        request.user = jwt.verify(token, secret);
        next();
    }
    catch (err){
        return response.status(401).json({message: 'Invalid JWT'})
    }
}
