const express = require("express")
const mongoose = require("mongoose")
const app = express();

const { port } = require("./config/server")

const authRouter = require('./routers/authRouter')
const usersRouter = require('./routers/usersRouter')
const notesRouter = require('./routers/notesRouter')

mongoose.connect(`mongodb+srv://PavloShabatin:12345qwerty@cluster0.x5wfy.mongodb.net/NODEJS_HW2?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
    useCreateIndex: true
})

app.use(express.json())
app.use('/api', authRouter)
app.use('/api', usersRouter)
app.use('/api', notesRouter)

app.listen(port, () => {
    console.log(`Server is working on ${port}`)
})

