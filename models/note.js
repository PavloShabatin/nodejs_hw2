const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = mongoose.model('note', new Schema({
    userId: {
        required: true,
        type: String
    },
    completed: {
        required: true,
        type: Boolean
    },
    text: {
        required: true,
        type: String
    },
    createdDate: {
        required: true,
        type: Date
    }
}))
