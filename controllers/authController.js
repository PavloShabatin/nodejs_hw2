const jwt = require('jsonwebtoken')
const User = require('../models/user')
const Credential = require('../models/credentials')
const {secret} = require('../config/auth')

module.exports.register = (request, response) => {
    const {username, password} = request.body

    const user = new User({username, createdDate: new Date()});
    const credential = new Credential({username, password})
    credential.save()
        .then(() => {
            if (!username || !password) {
                return response.status(400).json({message: 'string'})
            }
            user.save()
                .then(() => {
                    return response.json({message: 'Success'})
                })
                .catch(err => {
                    return response.status(500).json({message: 'string'})
                })
        })
        .catch(err => {
            console.log(err.message)
            response.status(500).json({message: 'string'})
        })
}

module.exports.login = (request, response) => {
    const {username, password} = request.body

    Credential.findOne({username, password}).exec()
        .then(user => {
            if (!user) {
                return response.status(400).json({message: 'No user with such username or password found'})
            }
            response.json({message: 'Success', jwt_token: jwt.sign(JSON.stringify(user), secret)})
        })
        .catch(err => {
            return response.status(500).json({message: err.message})
        })
}
