const jwt = require('jsonwebtoken')
const User = require('../models/user')
const Credential = require('../models/credentials')

module.exports.getUser = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const username = jwt.decode(token).username
    User.findOne({username}, {__v: 0}).exec()
        .then(user => {
            if (!user) {
                return response.status(400).json({message: 'string'})
            }
            return response.json({user: user})
        })
        .catch(err => {
            return response.status(500).json({status: err.message})
        })
}

module.exports.deleteUser = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const username = jwt.decode(token).username
    User.remove({username}).exec()
        .then(user => {
            if (user.deletedCount === 0) {
                return response.status(400).json({message: 'string'})
            }
            return response.json({message: "Success"})
        })
        .catch(err => {
            return response.status(500).json({status: err.message})
        })
    Credential.remove({username}).exec()
        .then(user => {
            if (user.deletedCount === 0) {
                return response.status(400).json({message: 'string'})
            }
            return response.json({message: "Success"})
        })
        .catch(err => {
            return response.status(500).json({status: err.message})
        })
}

module.exports.changePassword = (request, response) => {
    const {oldPassword, newPassword} = request.body
    const update = {password: newPassword}
    const [, token] = request.headers.authorization.split(' ');
    const username = jwt.decode(token).username
    Credential.findOneAndUpdate({username}, update).exec()
        .then(user => {
            if(!user || oldPassword !== user.password){
                console.log(user)
                return response.status(400).json({message: 'string'})
            }
            return response.json({message: "Success"})
        })
        .catch(err => {
            return response.status(500).json({message: 'string'})
        })
}
