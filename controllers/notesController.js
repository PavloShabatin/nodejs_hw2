const jwt = require('jsonwebtoken')
const Note = require('../models/note')

module.exports.getNotes = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const userId = jwt.decode(token)._id
    Note.find({userId}, {__v: 0}).exec()
        .then(notes => {
            if (!notes) {
                return response.status(400).json({message: 'string'})
            }
            return response.json({notes})
        })
        .catch(err => {
            return response.status(500).json({message: 'string'})
        })
}

module.exports.addNote = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const {text} = request.body
    const userId = jwt.decode(token)._id
    const note = new Note({userId, completed: false, text, createdDate: new Date()})
    note.save()
        .then(() => {
            return response.json({message: 'Success'})
        })
        .catch((err) => {
            return response.status(500).json({message: 'string'})
        })
}

module.exports.getNoteById = (request, response) => {
    Note.findById(request.params.id, {__v: 0}).exec()
        .then(note => {
            if (!note) {
                return response.status(400).json({message: 'string'})
            }
            return response.json({note: note})
        })
        .catch(err => {
            return response.status(500).json({message: 'string'})
        })
}

module.exports.updateNoteById = (request, response) => {
    const update = {text: request.body.text}
    Note.findByIdAndUpdate(request.params.id, update).exec()
        .then(note => {
            if (!note) {
                return response.status(400).json({message: 'string'})
            }
            return response.json({message: 'Success'})
        })
        .catch(err => {
            return response.status(500).json({message: 'string'})
        })
}

module.exports.checkNote = (request, response) => {
    Note.findById(request.params.id).exec()
        .then(note => {
            if (!note) {
                return response.status(400).json({message: 'string'})
            }
            if (note.completed) {
                Note.findByIdAndUpdate(request.params.id, {completed: false}).exec()
                    .then(note => {
                        if (!note) {
                            return response.status(400).json({message: 'string'})
                        }
                        return response.json({message: 'Success'})
                    })
                    .catch(err => {
                        return response.status(500).json({message: 'string'})
                    })
            } else {
                Note.findByIdAndUpdate(request.params.id, {completed: true}).exec()
                    .then(note => {
                        if (!note) {
                            return response.status(400).json({message: 'string'})
                        }
                        return response.json({message: 'Success'})
                    })
                    .catch(err => {
                        return response.status(500).json({message: 'string'})
                    })
            }
        })
        .catch(err => {
            return response.status(500).json({message: 'string'})
        })
}

module.exports.deleteNoteById = (request, response) => {
    Note.remove({_id: request.params.id}).exec()
        .then(note => {
            console.log(note)
            if (note.deletedCount === 0) {
                return response.status(400).json({message: 'string'})
            }
            return response.json({message: 'Success'})
        })
        .catch(err => {
            return response.status(500).json({message: 'string'})
        })
}
